package web

object Life {
  def step(s: Int, rows: Int, cols: Int, board: Seq[Seq[Int]]): Seq[Seq[Int]] = {
    def neighborsSum(i: Int, j: Int): Int = {
      val xs = for {
        i1 <- i-1 to i+1; j1 <- j-1 to j+1; if i1 != i || j1 != j
        if 0 <= i1 && i1 < rows; if 0 <= j1 && j1 < cols
      } yield board(i1)(j1)
      xs.sum
    }
    for (i <- 0 until rows) yield {
      for (j <- 0 until cols) yield {
        val s = 2*neighborsSum(i, j) + board(i)(j)
        if (4 < s && s < 8) 1 else 0
      }
    }
  }

}
