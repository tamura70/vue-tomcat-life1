package web

/*
   https://github.com/json4s/json4s
 */

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

object Command {
  /* https://stackoverflow.com/questions/23348480/json4s-convert-type-to-jvalue */
  def encodeJson(src: AnyRef): JValue = {
    import org.json4s.{ Extraction, NoTypeHints }
    import org.json4s.JsonDSL.WithDouble._
    import org.json4s.jackson.Serialization
    implicit val formats = Serialization.formats(NoTypeHints)
    Extraction.decompose(src)
  }
 
  def handler(command: String, params: Map[String,Seq[String]]): JValue = command match {
    case "date" => {
      val fmt = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
      val str = fmt.format(new java.util.Date)
      JString(str)
    }
    case "life-step" => {
      val s: Int =
        params.get("s") match { case Some(Seq(s)) => s.toInt; case _ => 1 }
      val rows: Int =
        params.get("m") match { case Some(Seq(m)) => m.toInt; case _ => 10 }
      val cols: Int =
        params.get("n") match { case Some(Seq(n)) => n.toInt; case _ => 10 }
      val board: Seq[Seq[Int]] = params.get("b") match {
        case Some(Seq(b)) =>
          for (r <- b.split("[^01]+").toSeq) yield
            for (c <- r.split("").toSeq) yield
              if (c == "0") 0 else 1
        case _ =>
          throw new IllegalArgumentException("parameter b is not defined")
      }
      encodeJson(Life.step(s, rows, cols, board))
    }
    case "sat4j" => {
      params.get("cnf") match {
        case Some(Seq(dimacsString)) => {
          val sat4j = new Sat4j(dimacsString)
          val output = sat4j.solve()
          JString(output)
        }
        case _ => {
          JNull
        }
      }
    }
    case _ => {
      JNull
    }
  }

  def exec(command: String, params: Map[String,Seq[String]]): Option[String] = {
    println(s"Command.exec: $command, $params")
    val result: JValue = handler(command, params)
    if (result == JNull) {
      None
    } else {
      val json = 
        ("command" -> command) ~
        ("params" -> params) ~
        ("result" -> result)
      Some(compact(render(json)))
    }
  }
}

