package web

import org.sat4j.minisat.SolverFactory
import org.sat4j.reader.Reader
import org.sat4j.reader.DimacsReader
import org.sat4j.specs.IProblem
import org.sat4j.specs.ISolver
import org.sat4j.specs.ContradictionException
import org.sat4j.specs.TimeoutException
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

class Sat4j(dimacsString: String) {
  val timeout = 10000

  def solve(): String = {
    val sb = new StringBuilder
    val sat4jSolver: ISolver = SolverFactory.newDefault()
    val reader: Reader = new DimacsReader(sat4jSolver)
    try {
      val str = dimacsString.replaceAll("""\s*\r?\n\s*""", "\n")
      val in = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))
      val problem: IProblem = reader.parseInstance(in)
      sat4jSolver.setTimeoutMs(timeout)
      if (problem.isSatisfiable()) {
        sb.append("SAT\n")
        val model = problem.model()
        for (i <- 0 until model.length) {
          sb.append(model(i))
          sb.append(" ")
        }
        sb.append("0\n")
      } else {
        sb.append("UNSAT\n")
      }
    } catch {
      case e: ContradictionException => {
        sb.append("UNSAT\n")
      }
      case e: TimeoutException => {
        sb.append("TIMEOUT\n")
      }
      case e: Exception => {
        sb.append(s"ERROR ${e.getMessage}\n")
        println(e)
      }
    } finally {
      sat4jSolver.setTimeoutMs(0)
      sat4jSolver.reset
    }
    sb.toString
  }
}
