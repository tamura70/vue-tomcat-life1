package:
	cd front; npm run build
	rm -rf src/main/webapp/css src/main/webapp/js
	cp -pr front/dist/* src/main/webapp/
	sbt package

run:
	cd front; npm run build
	rm -rf src/main/webapp/css src/main/webapp/js
	cp -pr front/dist/* src/main/webapp/
	sbt ~Tomcat/start

clean:
	rm -rf src/main/webapp/css src/main/webapp/js
	rm -rf front/dist
	rm -rf .bsp project/project/ project/target/ target/

realclean: clean
	rm -rf front/node_modules/
